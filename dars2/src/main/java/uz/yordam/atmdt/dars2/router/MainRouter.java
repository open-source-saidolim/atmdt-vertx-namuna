package uz.yordam.atmdt.dars2.router;

import io.vertx.core.Vertx;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import uz.yordam.atmdt.dars2.controller.MainController;

public class MainRouter {

    private MainController mainController;

    public MainRouter() {
        mainController = new MainController();
    }

    public Router getRouter(Vertx vertx) {

        Router router = Router.router(vertx);

        router.route("/kurs").handler(mainController::kurs);
        router.route("/stipendiya").handler(mainController::stipendiya);

        // Agar sahifa topilmasa
        router.route().handler(this::page404);

        return router;
    }

    /**
     * Agar sahifa topilmasa
     *
     * @param routingContext
     */
    public void page404(RoutingContext routingContext) {
        routingContext
                .response()
                .setStatusCode(404)
                .end("Xato 404");
    }
}
