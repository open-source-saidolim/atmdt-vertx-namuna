package uz.yordam.atmdt.dars2;

import io.vertx.core.AbstractVerticle;
import io.vertx.ext.web.Router;
import uz.yordam.atmdt.dars2.router.MainRouter;

public class MainVerticle extends AbstractVerticle {

    @Override
    public void start() {

        Router router = new MainRouter().getRouter(vertx);

        vertx.createHttpServer()
                .requestHandler(router)
                .listen(8080);

//                        routerContext -> routerContext
//                                .response()
//                                .end("salom")
    }
}

