package uz.yordam.atmdt.dars2.controller;

import io.vertx.ext.web.RoutingContext;

public class MainController {

    public void kurs(RoutingContext routingContext) {
        routingContext.response().end("Kurs 8300 ");
    }

    public void stipendiya(RoutingContext routingContext) {
        routingContext.response().end("Stipendiya 744'000 ");
    }
}
