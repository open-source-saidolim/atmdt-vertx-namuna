package uz.yordam.atmdt.dars1;

import io.vertx.core.AbstractVerticle;

public class MainVerticle extends AbstractVerticle {

    @Override
    public void start() {

        vertx.createHttpServer()
                .requestHandler(
                        routerContext -> routerContext
                                .response()
                                .end("salom")
                )
                .listen(8080);

    }
}

